-- phpMyAdmin SQL Dump
-- version 5.0.4
-- https://www.phpmyadmin.net/
--
-- Servidor: localhost
-- Tiempo de generación: 03-03-2021 a las 05:18:59
-- Versión del servidor: 10.4.17-MariaDB
-- Versión de PHP: 7.4.15

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `empleados`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `empleados`
--

CREATE TABLE `empleados` (
  `identificacion` varchar(20) NOT NULL,
  `tipodedocumento` varchar(40) NOT NULL,
  `primerapellido` varchar(20) NOT NULL,
  `segundoapellido` varchar(20) NOT NULL,
  `primernombre` varchar(20) NOT NULL,
  `otrosnombres` varchar(50) DEFAULT NULL,
  `correoelectronico` varchar(300) NOT NULL,
  `pais` varchar(40) NOT NULL,
  `fechaingreso` date NOT NULL,
  `area` varchar(60) NOT NULL,
  `estado` varchar(30) NOT NULL,
  `fecharegistro` datetime NOT NULL,
  `fechaedicion` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Volcado de datos para la tabla `empleados`
--

INSERT INTO `empleados` (`identificacion`, `tipodedocumento`, `primerapellido`, `segundoapellido`, `primernombre`, `otrosnombres`, `correoelectronico`, `pais`, `fechaingreso`, `area`, `estado`, `fecharegistro`, `fechaedicion`) VALUES
('1069763864', 'Permiso Especial', 'TRIANA ', 'VARGAS', 'JHONS', 'EDUARD', 'jhons.triana.2@cidenet.com.co', 'Colombia', '2021-03-02', 'Compras', 'Activo', '2021-03-03 02:47:20', '2021-03-03 03:10:59'),
('12345', 'Cedula de Ciudadania', 'JOHAN', 'SEBASTIAN', 'LOZANO', '', 'lozano.johan.2@cidenet.com.co', 'Colombia', '2021-03-02', 'Financiera', 'Activo', '2021-03-03 03:16:09', '2021-03-03 03:16:09'),
('1423423f', 'Pasaporte', 'CAMILO', 'BELTRAN', 'VARGAS', '', 'vargas.camilo.5@cidenet.com.co', 'Colombia', '2021-03-02', 'Operacion', 'Activo', '2021-03-03 03:17:43', '2021-03-03 03:17:43'),
('23424', 'Cedula de Ciudadania', 'TRIANA', 'VARGAS', 'KAROL', '', 'karo.triana.8@cidenet.com.us', 'Estados Unidos', '2021-03-02', 'Operacion', 'Activo', '2021-03-03 03:18:54', '2021-03-03 03:21:32'),
('435313', 'Cedula de Extranjeria', 'TRIANA', 'VARGAS', 'STEFFANY', 'LORENA', 'steffany.triana.4@cidenet.com.co', 'Colombia', '2021-03-01', 'Compras', 'Activo', '2021-03-03 03:17:11', '2021-03-03 03:17:11'),
('453322', 'Permiso Especial', 'LUZ', 'EDITH', 'VARGAS', '', 'vargas.luz.3@cidenet.com.us', 'Estados Unidos', '2021-03-03', 'Operacion', 'Activo', '2021-03-03 03:16:37', '2021-03-03 03:16:37'),
('5123451', 'Pasaporte', 'JAIRO', 'MARTINEZ', 'LOZANO', '', 'lozano.jairo.6@cidenet.com.us', 'Estados Unidos', '2021-03-02', 'Servicios Varios', 'Activo', '2021-03-03 03:18:26', '2021-03-03 03:18:26'),
('524311', 'Cedula de Ciudadania', 'VARGAS', 'CLAVIJO', 'EDNA', '', 'edn.vargas.11@cidenet.com.us', 'Estados Unidos', '2021-02-16', 'Operacion', 'Activo', '2021-03-03 03:31:50', '2021-03-03 03:31:50'),
('53453534', 'Cedula de Extranjeria', 'TRIANA', 'VARGAS', 'JHON', '', 'jhon.triana.8@cidenet.com.us', 'Estados Unidos', '2021-03-03', 'Servicios Varios', 'Activo', '2021-03-03 03:22:59', '2021-03-03 03:22:59'),
('54353', 'Pasaporte', 'JAIRO', 'MELENDEZ', 'OJEDA', '', 'ojeda.jairo.10@cidenet.com.us', 'Estados Unidos', '2021-03-01', 'Compras', 'Activo', '2021-03-03 03:31:19', '2021-03-03 03:31:19'),
('545353', 'Cedula de Extranjeria', 'JAIRO', 'CORDOBA', 'MELENDEZ', '', 'melendez.jairo.9@cidenet.com.co', 'Colombia', '2021-03-02', 'Operacion', 'Activo', '2021-03-03 03:30:42', '2021-03-03 03:30:42');

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `empleados`
--
ALTER TABLE `empleados`
  ADD PRIMARY KEY (`identificacion`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
