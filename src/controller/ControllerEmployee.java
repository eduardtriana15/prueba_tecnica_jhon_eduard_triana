package controller;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.List;
import javax.swing.event.TableModelEvent;
import javax.swing.event.TableModelListener;
import view.Principal;
import model.Employee;
import model.EmployeeDAO;
import utilities.ProveedorDeDatosDePaginacion;

//Controlador de empleados encargado de gestionar la parte de la logica para la paginacion de los registros 
public class ControllerEmployee implements ActionListener, TableModelListener{

    //Inicializamos las variables requeridad para la paginacion
    private final Principal view;//Ventana donde se listan todos los empleados
    private final EmployeeDAO model;//Modelo que permite gestionar las consultas SQL 
    private final PaginadorDeTabla<Employee> paginadorDeTabla;    
    

    //metodo que permite la realizacion de los evetos para la seleccion de cantidad de registros por pagina y actualizacion en el Jtable
    public final void events(){
        view.pageJComboBox.addActionListener(this);
        view.tablaEmpleados.getModel().addTableModelListener(this);
    }
    

    public ControllerEmployee(Principal view){
        //llamaremos las variables inicializadas desde el inicio de la clase
        this.view = view;
        model = new EmployeeDAO();
        
        //llamamos un metodo que nos permite proveer los datos del datasource, esto permitira tener el total de filas y total de columnas
        ProveedorDeDatosDePaginacion<Employee> proveedorDeDatos = crearProveedorDeDatos();                  
        
        //Indicamos de cuantos registros se pueden trabajar por pagina en la tabla de empleados, que es obtenida en la vista principal
        paginadorDeTabla = new PaginadorDeTabla(this.view.tablaEmpleados, proveedorDeDatos, new int[]{5, 10, 20, 50, 75, 100}, 10);
        
//Creamos el listado de las filas permitidas
        paginadorDeTabla.crearListadoDeFilasPermitidas(view.paginationPanel);

        //Agregamos el combobox a los eventos de paginar
        view.pageJComboBox = paginadorDeTabla.getComboBoxPage();
       
        //Llamamos el metodo de eventos
        events();
        
       //indicamos por default un valor predeterminado 10 filas por hoja
        view.pageJComboBox.setSelectedItem(Integer.parseInt("10"));
        
    }
        
//Metodo que permite trabajar con la paginacion
    private ProveedorDeDatosDePaginacion<Employee> crearProveedorDeDatos() {
        
   //creamos un arreglo de tipo list
        List <Employee> list = model.selectEmployee();

        return new ProveedorDeDatosDePaginacion<Employee>() {
            //metodo que permite obtener el tamaño de la ista
            @Override
            public int getTotalRowCount() {
                return list.size();
            }

            //metodo que permite obtener las filas pasando como argumento el indice inical y el indice final
            @Override
            public List<Employee> getRows(int startIndex, int endIndex) {
                return list.subList(startIndex, endIndex);
            }
        };
    }

//Metodos que permiten trabajar conjuntamente con el combobox y la paginacion de la tabla de empleados
    @Override
    public void actionPerformed(ActionEvent ae) {

        Object evt = ae.getSource();
           
        paginadorDeTabla.eventCombobBox(view.pageJComboBox);
        
    }

    @Override
    public void tableChanged(TableModelEvent tme) {
        paginadorDeTabla.refreshPageButtonPanel();
    }



    
    
    
}
