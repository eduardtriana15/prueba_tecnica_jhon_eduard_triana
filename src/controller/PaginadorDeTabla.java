
package controller;


import utilities.ModeloDeTabla;
import javax.swing.*;
import javax.swing.table.TableModel;
import java.awt.*;
import java.util.List;
import utilities.ProveedorDeDatosDePaginacion;

public class PaginadorDeTabla<T> {
    
  
    private JPanel panelPaginacionBotones;
    
    

    private JComboBox<Integer> listaLimiteDeFilas;    
    

    private final JTable tablaEmployee;
    
    private final ProveedorDeDatosDePaginacion<T> dataProvider;
    

    private final int[] numeroDeFilasPermitidas;
    
  
    private int filasPermitidasPorDefecto;
    
    private int paginaActual = 1;
    

    private ModeloDeTabla modeloDeTabla;
    

    private final int limiteBotonesPaginadores = 9;
    
    
    public PaginadorDeTabla(JTable tabla, ProveedorDeDatosDePaginacion<T> proveedorDeDatos,
                                    int[] numeroDeFilasPermitidas, int filasPermitidasPorDefecto) {
        this.tablaEmployee = tabla;
        this.dataProvider = proveedorDeDatos;
        this.numeroDeFilasPermitidas = numeroDeFilasPermitidas;
        this.filasPermitidasPorDefecto = filasPermitidasPorDefecto;
        init();
        
    }    

    private void init() {        
        inicializarModeloDeTabla();
        paginar();
    }
    
    private void inicializarModeloDeTabla() {
        TableModel model = tablaEmployee.getModel();
        if (!(model instanceof ModeloDeTabla)) {
            throw new IllegalArgumentException("TableModel must be a subclass of ObjectTableModel");
        }        
        modeloDeTabla = (ModeloDeTabla) model;
    }


    public void crearListadoDeFilasPermitidas(JPanel panelPaginador){
        
        panelPaginacionBotones = new JPanel(new GridLayout(1, limiteBotonesPaginadores, 3, 3));
        panelPaginador.add(panelPaginacionBotones);        

        if (numeroDeFilasPermitidas != null) {
            
            Integer array[] = new Integer[numeroDeFilasPermitidas.length];
            for(int i=0; i<numeroDeFilasPermitidas.length; i++){
                array[i] = numeroDeFilasPermitidas[i];
            }
            listaLimiteDeFilas = new JComboBox<>(array);
                        
            panelPaginador.add(Box.createHorizontalStrut(15));
            JLabel lbl =new JLabel("Número de Filas: ");
            lbl.setForeground(Color.white);
            panelPaginador.add(lbl);
            panelPaginador.add(listaLimiteDeFilas);
            
        }
        
        
    }    
    
    public void eventCombobBox(JComboBox<Integer> pageComboBox){

        int currentPageStartRow = ((paginaActual - 1) * filasPermitidasPorDefecto) + 1;
        filasPermitidasPorDefecto = (Integer) pageComboBox.getSelectedItem();
        paginaActual = ((currentPageStartRow - 1) / filasPermitidasPorDefecto) + 1;
        paginar();        
    
    }
    
    

    public void refreshPageButtonPanel() {
        
       
        panelPaginacionBotones.removeAll();
        
       
        int totalRows = dataProvider.getTotalRowCount();
        
      
        int pages = (int) Math.ceil((double) totalRows / filasPermitidasPorDefecto);
        
        
        ButtonGroup buttonGroup = new ButtonGroup();
        
      
        if (pages > limiteBotonesPaginadores) {
            
            addPageButton(panelPaginacionBotones, buttonGroup, 1);
            if (paginaActual > (pages - ((limiteBotonesPaginadores + 1) / 2))) {
                panelPaginacionBotones.add(createEllipsesComponent());
                addPageButtonRange(panelPaginacionBotones, buttonGroup, pages - limiteBotonesPaginadores + 3, pages);
            } else if (paginaActual <= (limiteBotonesPaginadores + 1) / 2) {
                addPageButtonRange(panelPaginacionBotones, buttonGroup, 2, limiteBotonesPaginadores - 2);
                panelPaginacionBotones.add(createEllipsesComponent());
                addPageButton(panelPaginacionBotones, buttonGroup, pages);
            } else {
                panelPaginacionBotones.add(createEllipsesComponent());
                int start = paginaActual - (limiteBotonesPaginadores - 4) / 2;
                int end = start + limiteBotonesPaginadores - 5;
                addPageButtonRange(panelPaginacionBotones, buttonGroup, start, end);
                panelPaginacionBotones.add(createEllipsesComponent());
                addPageButton(panelPaginacionBotones, buttonGroup, pages);
            }
        } else {
            addPageButtonRange(panelPaginacionBotones, buttonGroup, 1, pages);
        }
        panelPaginacionBotones.getParent().validate();
        panelPaginacionBotones.getParent().repaint();
    }    
    
    private Component createEllipsesComponent() {
        return new JLabel("...", SwingConstants.CENTER);
    }

    private void addPageButtonRange(JPanel parentPanel, ButtonGroup buttonGroup, int start, int end) {

        int init = start;
        for (start=init; start <= end; start++) {
            addPageButton(parentPanel, buttonGroup, start);
        }
    }

    private void addPageButton(JPanel parentPanel, ButtonGroup buttonGroup, int pageNumber) {
        

        JToggleButton toggleButton = new JToggleButton(Integer.toString(pageNumber));
        
     
        toggleButton.setMargin(new Insets(1, 3, 1, 3));
        
       
        buttonGroup.add(toggleButton);
        
        
        parentPanel.add(toggleButton);
        
        
        if (pageNumber == paginaActual) {
            toggleButton.setSelected(true);
        }
        
     
        toggleButton.addActionListener(ae -> {
            paginaActual = Integer.parseInt(ae.getActionCommand());
            paginar();
        });
    }

    
    
    private void paginar() {
        int startIndex = (paginaActual - 1) * filasPermitidasPorDefecto;
        int endIndex = startIndex + filasPermitidasPorDefecto;
        if (endIndex > dataProvider.getTotalRowCount()) {
            endIndex = dataProvider.getTotalRowCount();
        }
        List<T> rows = dataProvider.getRows(startIndex, endIndex);
        modeloDeTabla.setListRows(rows);
        modeloDeTabla.fireTableDataChanged();
    }
    
    
    public JComboBox<Integer> getComboBoxPage() {
        return listaLimiteDeFilas;
    }

    public void setComboBoxPage(JComboBox<Integer> comboBoxPage) {
        this.listaLimiteDeFilas = comboBoxPage;
    }
    
    
    
}