package model;

import javax.sql.DataSource;
import org.apache.commons.dbcp2.BasicDataSource;

//Clase que permite la conexion con la fuente de datos, en este caso con el gestor de bases de datos MySQL

public class Connect {

    
    private final String DB="empleados";
    private final String URL="jdbc:mysql://localhost:3306/"+DB+"?useUnicode=true&useJDBCCompliantTimezoneShift=true&useLegacyDatetimeCode=false&serverTimezone=UTC";
    private final String USER="root";
    private final String PASS="";    
 
    public DataSource dataSource=null;
    
    public Connect(){

        inicializaDataSource();

    }


    //Inicializamos el dataSource para realizacion una conexion segura y estable con la base de datos
    private void inicializaDataSource() {

        try{

            BasicDataSource basicDataSource = new BasicDataSource();

            basicDataSource.setDriverClassName("com.mysql.cj.jdbc.Driver");
            basicDataSource.setUsername(USER);
            basicDataSource.setPassword(PASS);
            basicDataSource.setUrl(URL);
            basicDataSource.setMaxTotal(50);

            dataSource = basicDataSource;
        
        }catch(Exception ex){
            System.out.println("error "+ex.getMessage());
        }

    }

}
