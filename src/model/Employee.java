package model;

import java.sql.Date;
import java.sql.Timestamp;

//Modelo de empleados donde indicamos los datos para trabajar conjuntamente con la base de datos, la vistas y los controladores..

public class Employee{
    //inicializamos las variables
private String identificacion;
private String tipodedocumento;
private String primerapellido;
private String segundoapellido;
private String primernombre;
private String otrosnombres;
private String correoelectronico;
private String pais;
private Date fechaingreso;
private String area;
private String estado;
private Timestamp fecharegistro;
private Timestamp fechaedicion;

//Constructores
    public Employee(String identificacion, String tipodedocumento, String primerapellido, String segundoapellido, String primernombre, String otrosnombres, String correoelectronico, String pais, String area, String estado, Timestamp fechaedicion) {
        this.identificacion = identificacion;
        this.tipodedocumento = tipodedocumento;
        this.primerapellido = primerapellido;
        this.segundoapellido = segundoapellido;
        this.primernombre = primernombre;
        this.otrosnombres = otrosnombres;
        this.correoelectronico = correoelectronico;
        this.pais = pais;
        this.area = area;
        this.estado = estado;
        this.fechaedicion = fechaedicion;
    }





    public Employee(String identificacion, String tipodedocumento, String primerapellido, String segundoapellido, String primernombre, String otrosnombres, String correoelectronico, String pais, Date fechaingreso, String area, String estado, Timestamp fecharegistro, Timestamp fechaedicion) {
        this.identificacion = identificacion;
        this.tipodedocumento = tipodedocumento;
        this.primerapellido = primerapellido;
        this.segundoapellido = segundoapellido;
        this.primernombre = primernombre;
        this.otrosnombres = otrosnombres;
        this.correoelectronico = correoelectronico;
        this.pais = pais;
        this.fechaingreso = fechaingreso;
        this.area = area;
        this.estado = estado;
        this.fecharegistro = fecharegistro;
        this.fechaedicion = fechaedicion;
    }

 
//Metodos get y setter



    public Timestamp getFechaedicion() {
        return fechaedicion;
    }

    public void setFechaedicion(Timestamp fechaedicion) {
        this.fechaedicion = fechaedicion;
    }



    public Employee() {
    }

    public String getIdentificacion() {
        return identificacion;
    }

    public void setIdentificacion(String identificacion) {
        this.identificacion = identificacion;
    }

    public String getTipodedocumento() {
        return tipodedocumento;
    }

    public void setTipodedocumento(String tipodedocumento) {
        this.tipodedocumento = tipodedocumento;
    }

    public String getPrimerapellido() {
        return primerapellido;
    }

    public void setPrimerapellido(String primerapellido) {
        this.primerapellido = primerapellido;
    }

    public String getSegundoapellido() {
        return segundoapellido;
    }

    public void setSegundoapellido(String segundoapellido) {
        this.segundoapellido = segundoapellido;
    }

    public String getPrimernombre() {
        return primernombre;
    }

    public void setPrimernombre(String primernombre) {
        this.primernombre = primernombre;
    }

    public String getOtrosnombres() {
        return otrosnombres;
    }

    public void setOtrosnombres(String otrosnombres) {
        this.otrosnombres = otrosnombres;
    }

    public String getCorreoelectronico() {
        return correoelectronico;
    }

    public void setCorreoelectronico(String correoelectronico) {
        this.correoelectronico = correoelectronico;
    }

    public String getPais() {
        return pais;
    }

    public void setPais(String pais) {
        this.pais = pais;
    }



    public Date getFechaingreso() {
        return fechaingreso;
    }

    public void setFechaingreso(Date fechaingreso) {
        this.fechaingreso = fechaingreso;
    }

    public String getArea() {
        return area;
    }

    public void setArea(String area) {
        this.area = area;
    }

    public String getEstado() {
        return estado;
    }

    public void setEstado(String estado) {
        this.estado = estado;
    }

    public Timestamp getFecharegistro() {
        return fecharegistro;
    }

    public void setFecharegistro(Timestamp fecharegistro) {
        this.fecharegistro = fecharegistro;
    }



    
}


