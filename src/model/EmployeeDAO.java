package model;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;


public class EmployeeDAO {

  //Variables que permiten realizar y trabajar con la obtencion de los datos proveientes de la base de datos
    private final Connect connection;
    private PreparedStatement pst;
    private ResultSet rs;
    
//Constructor
    public EmployeeDAO() {
        this.connection = new Connect();
    }

//Metodo que permite guardar un empleado
    public int GuardarEmpleao(Employee emp){
        int resultado=0;
        
        try {
        Connection connect = null;   
       
        String sql="INSERT INTO empleados (identificacion,tipodedocumento,primerapellido,segundoapellido,primernombre,otrosnombres,correoelectronico,pais,fechaingreso,area,estado,fecharegistro,fechaedicion)  "
                + "VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?)";
        connect = connection.dataSource.getConnection();
        pst = connect.prepareStatement(sql);
        
        pst.setString(1, emp.getIdentificacion());
        pst.setString(2, emp.getTipodedocumento());
        pst.setString(3, emp.getPrimerapellido());
        pst.setString(4, emp.getSegundoapellido());
        pst.setString(5, emp.getPrimernombre());
        pst.setString(6, emp.getOtrosnombres());
        pst.setString(7, emp.getCorreoelectronico());
        pst.setString(8, emp.getPais());
        pst.setDate(9, emp.getFechaingreso());
        pst.setString(10, emp.getArea());
        pst.setString(11, emp.getEstado());
        pst.setTimestamp(12, emp.getFecharegistro());
        pst.setTimestamp(13, emp.getFechaedicion()); 
        resultado = pst.executeUpdate();
        
        pst.close();
        
        } catch (SQLException ex) {
            ex.printStackTrace();
        }
        return resultado;
    }
    
    //Metodo que permite eliminar un empleado
    public int eliminarEmpleado(String Identificacion){
    int resultado=0;
    
            try {
        Connection connect = null;   
       
        String sql="DELETE FROM empleados WHERE identificacion=?";
        connect = connection.dataSource.getConnection();
        pst = connect.prepareStatement(sql);
        
        pst.setString(1,Identificacion);
        resultado = pst.executeUpdate();
        
        pst.close();
        
        } catch (SQLException ex) {
            ex.printStackTrace();
        }
        return resultado;
    
    }
    
    //Metodo que permite actualizar un empleado
    public int ActualizarEmpleado(Employee emp){
        int res=0;
         try {
        Connection connect = null;   
       
  
        String sql="UPDATE empleados SET tipodedocumento=?, primerapellido=?, segundoapellido=?, primernombre=?, otrosnombres=?, correoelectronico=?, pais=?, area=?, estado=?, fechaedicion=? "
                + "WHERE Identificacion=?";
        connect = connection.dataSource.getConnection();
        pst = connect.prepareStatement(sql);
        
                                
        pst.setString(1, emp.getTipodedocumento());
        pst.setString(2, emp.getPrimerapellido());
        pst.setString(3, emp.getSegundoapellido());
        pst.setString(4, emp.getPrimernombre());
        pst.setString(5, emp.getOtrosnombres());
        pst.setString(6, emp.getCorreoelectronico());
        pst.setString(7, emp.getPais());
        pst.setString(8, emp.getArea());
        pst.setString(9, emp.getEstado());
        pst.setTimestamp(10, emp.getFechaedicion());
        pst.setString(11, emp.getIdentificacion());
       res =  pst.executeUpdate();
        pst.close();
        
        } catch (SQLException ex) {
            ex.printStackTrace();
        } 
         return res;
    }

    //Metodo que permite obtener la cantidad de empleados, esto permitira trabajar conjuntamente la creacion de los correos de los empleados
    public int CantidadEmpleados() {
        int cantidadFilas=0;
        try {
         Connection connect = null;    
         connect = connection.dataSource.getConnection();
            String sql = "SELECT * FROM empleados";           
            pst = connect.prepareStatement(sql);
                  rs = pst.executeQuery();               
                   while(rs.next()){   
                       cantidadFilas++;
                }             
        } catch (SQLException ex) {
            Logger.getLogger(EmployeeDAO.class.getName()).log(Level.SEVERE, null, ex);
        }        
     return cantidadFilas;   
    }

    //Metodo que permite validar si existe o no un empleado de acuerdo con la identificacion o tipo de documento
        public int ValidarEmpleado(String id, String tipodoc) {
        int cantidadRegistros=0;
        try {
         Connection connect = null;    
         connect = connection.dataSource.getConnection();
            String sql = "SELECT * FROM empleados WHERE identificacion=? AND tipodedocumento=?";           
            pst = connect.prepareStatement(sql);
         pst.setString(1, id);
        pst.setString(2, tipodoc);
                  rs = pst.executeQuery();               
                   while(rs.next()){   
                       cantidadRegistros++;
                }             
        } catch (SQLException ex) {
            Logger.getLogger(EmployeeDAO.class.getName()).log(Level.SEVERE, null, ex);
        }        
     return cantidadRegistros;   
    }
    
        
    //Metodo que retorna un arrayList con toda la informacion de todos los empleados    
    public ArrayList<Employee> selectEmployee(){

        Connection connect = null;    
        ArrayList list = new ArrayList();
        Employee empleados;
        
        try{
            

            connect = connection.dataSource.getConnection();
            
            if(connect != null){
                
                String sql = "SELECT * FROM empleados";
                pst = connect.prepareStatement(sql);
                
                rs = pst.executeQuery();
                
                while(rs.next()){
                    empleados = new Employee();
                
                    empleados.setIdentificacion(rs.getString("identificacion"));
                    empleados.setTipodedocumento(rs.getString("tipodedocumento"));
                    empleados.setPrimerapellido(rs.getString("primerapellido"));
                    empleados.setSegundoapellido(rs.getString("segundoapellido"));
                    empleados.setPrimernombre(rs.getString("primernombre"));
                    empleados.setOtrosnombres(rs.getString("otrosnombres"));
                    empleados.setCorreoelectronico(rs.getString("correoelectronico"));
                    empleados.setPais(rs.getString("pais"));
                    empleados.setFechaingreso(rs.getDate("fechaingreso"));
                    empleados.setArea(rs.getString("area"));
                    empleados.setEstado(rs.getString("estado"));
                    empleados.setFecharegistro(rs.getTimestamp("fecharegistro"));   
       empleados.setFechaedicion(rs.getTimestamp("fechaedicion"));   
                    list.add(empleados);
                    
                }
                
            }
            
        }catch(SQLException ex){
        
            System.out.println(ex.getMessage());
        
        }finally{
            try{
                if(connect!=null){
                    connect.close();
                }
            }catch(SQLException ex){
                System.out.println("method: selectEmployee(), class: EmployeeDAO "+ex.getMessage());
            }
        }
        
        
        return list;
        
    }    
    
    
}
