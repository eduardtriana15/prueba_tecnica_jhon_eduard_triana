package utilities;

import javax.swing.table.AbstractTableModel;
import java.util.ArrayList;
import java.util.List;

//Clase Abstracta que permite listar las filas mediante un arraylist

public abstract class ModeloDeTabla<T> extends AbstractTableModel {

    private List<T> listadoDeFilas = new ArrayList<>();

    
    //Metdos que permite obtener las filas, cantidad de filas, obtener el nombre de la columna y obtener un valor de la tabla (mediante la posicion de la fila y columna)
    
    public List<T> getListRows() {
        return listadoDeFilas;
    }

    public void setListRows(List<T> listadoDeFilas) {
        this.listadoDeFilas = listadoDeFilas;
    }

    @Override
    public int getRowCount() {
        return listadoDeFilas.size();
    }
    
    
    @Override
    public Object getValueAt(int filas, int columnas) {
        T t = listadoDeFilas.get(filas);
        return getValueAt(t, columnas);
    }

    public abstract Object getValueAt(T t, int columnas);

    @Override
    public abstract String getColumnName(int columnas);

}