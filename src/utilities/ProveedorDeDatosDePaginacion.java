package utilities;

import java.util.List;

//Interfaz con las variables para obtener las filas 

public interface ProveedorDeDatosDePaginacion<T> {
    int getTotalRowCount();
    List <T> getRows(int startIndex, int endIndex);
}