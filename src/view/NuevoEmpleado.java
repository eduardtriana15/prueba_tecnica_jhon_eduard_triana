/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package view;

import java.sql.Timestamp;
import java.util.Calendar;
import javax.swing.JOptionPane;
import model.Employee;
import model.EmployeeDAO;
import java.text.Normalizer;

/**
 *
 * @author eduardtriana
 */
public class NuevoEmpleado extends javax.swing.JFrame {
  //Inializamos variables para trabajar con la gestion de crear empleados
    EmployeeDAO registrar;
    String paisSeleccionado="";
    public NuevoEmpleado() {
             registrar = new EmployeeDAO();

        initComponents();
         this.setLocationRelativeTo(null);
         
         //obtenemos fecha del sistema 
         Calendar c = Calendar.getInstance();
                    java.util.Date fechaSistema = new java.util.Date();
            this.txtFechaRegistro.setText(""+c.get(Calendar.DATE)+"/"+c.get(Calendar.MONTH)+"/"+c.get(Calendar.YEAR)+"  "+fechaSistema.getHours()+":"+fechaSistema.getMinutes()+":"+fechaSistema.getSeconds());

      //por medio de estas lineas de codigo, indicamos el minimo y maximo de fechas del JDateChooser
      java.util.Date fechaminima= RestarMes(fechaSistema,-1); 
      txtFechaIngreso.setMinSelectableDate(fechaminima);
      txtFechaIngreso.setMaxSelectableDate(fechaSistema);
    }


    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jPanel1 = new javax.swing.JPanel();
        jButton1 = new javax.swing.JButton();
        jLabel1 = new javax.swing.JLabel();
        jSeparator1 = new javax.swing.JSeparator();
        jLabel2 = new javax.swing.JLabel();
        jLabel3 = new javax.swing.JLabel();
        jLabel4 = new javax.swing.JLabel();
        jLabel5 = new javax.swing.JLabel();
        jLabel6 = new javax.swing.JLabel();
        jLabel7 = new javax.swing.JLabel();
        jLabel8 = new javax.swing.JLabel();
        jLabel9 = new javax.swing.JLabel();
        jLabel10 = new javax.swing.JLabel();
        jLabel11 = new javax.swing.JLabel();
        jLabel12 = new javax.swing.JLabel();
        jLabel13 = new javax.swing.JLabel();
        txtID = new javax.swing.JTextField();
        txtSegundoApellido = new javax.swing.JTextField();
        txtPrimerNombre = new javax.swing.JTextField();
        txtOtrosNombres = new javax.swing.JTextField();
        ComboTD = new javax.swing.JComboBox<>();
        txtEmail = new javax.swing.JTextField();
        ComboPais = new javax.swing.JComboBox<>();
        txtEstado = new javax.swing.JTextField();
        btnGuardar = new javax.swing.JToggleButton();
        btnCancelar = new javax.swing.JToggleButton();
        txtFechaIngreso = new com.toedter.calendar.JDateChooser();
        ComboArea = new javax.swing.JComboBox<>();
        jLabel14 = new javax.swing.JLabel();
        txtFechaRegistro = new javax.swing.JTextField();
        txtPrimerApellido = new javax.swing.JTextField();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        setUndecorated(true);

        jPanel1.setBackground(java.awt.SystemColor.activeCaption);
        jPanel1.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyReleased(java.awt.event.KeyEvent evt) {
                jPanel1KeyReleased(evt);
            }
        });

        jButton1.setIcon(new javax.swing.ImageIcon(getClass().getResource("/images/icons8-cerrar-ventana-80 (1).png"))); // NOI18N
        jButton1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton1ActionPerformed(evt);
            }
        });

        jLabel1.setFont(new java.awt.Font("Dialog", 1, 24)); // NOI18N
        jLabel1.setForeground(new java.awt.Color(255, 255, 255));
        jLabel1.setText("Crear Empleado");

        jSeparator1.setBackground(new java.awt.Color(255, 255, 255));

        jLabel2.setIcon(new javax.swing.ImageIcon(getClass().getResource("/images/icons8-guardar-como-40.png"))); // NOI18N

        jLabel3.setForeground(new java.awt.Color(255, 255, 255));
        jLabel3.setText("Identificacion (*)");

        jLabel4.setForeground(new java.awt.Color(255, 255, 255));
        jLabel4.setText("Tipo de Documento (*)");

        jLabel5.setForeground(new java.awt.Color(255, 255, 255));
        jLabel5.setText("Primer Apellido (*)");

        jLabel6.setForeground(new java.awt.Color(255, 255, 255));
        jLabel6.setText("Segundo Apellido (*)");

        jLabel7.setForeground(new java.awt.Color(255, 255, 255));
        jLabel7.setText("Primer Nombre (*)");

        jLabel8.setForeground(new java.awt.Color(255, 255, 255));
        jLabel8.setText("Otros Nombres");

        jLabel9.setForeground(new java.awt.Color(255, 255, 255));
        jLabel9.setText("Correo Electronico (*)");

        jLabel10.setForeground(new java.awt.Color(255, 255, 255));
        jLabel10.setText("Pais (*)");

        jLabel11.setForeground(new java.awt.Color(255, 255, 255));
        jLabel11.setText("Fecha Ingreso (*)");

        jLabel12.setForeground(new java.awt.Color(255, 255, 255));
        jLabel12.setText("Area (*)");

        jLabel13.setForeground(new java.awt.Color(255, 255, 255));
        jLabel13.setText("Estado (*)");

        txtID.setForeground(new java.awt.Color(0, 0, 0));
        txtID.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                txtIDKeyPressed(evt);
            }
            public void keyTyped(java.awt.event.KeyEvent evt) {
                txtIDKeyTyped(evt);
            }
        });

        txtSegundoApellido.setForeground(new java.awt.Color(0, 0, 0));
        txtSegundoApellido.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                txtSegundoApellidoKeyPressed(evt);
            }
            public void keyTyped(java.awt.event.KeyEvent evt) {
                txtSegundoApellidoKeyTyped(evt);
            }
        });

        txtPrimerNombre.setForeground(new java.awt.Color(0, 0, 0));
        txtPrimerNombre.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                txtPrimerNombreKeyPressed(evt);
            }
            public void keyTyped(java.awt.event.KeyEvent evt) {
                txtPrimerNombreKeyTyped(evt);
            }
        });

        txtOtrosNombres.setForeground(new java.awt.Color(0, 0, 0));
        txtOtrosNombres.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                txtOtrosNombresKeyPressed(evt);
            }
            public void keyTyped(java.awt.event.KeyEvent evt) {
                txtOtrosNombresKeyTyped(evt);
            }
        });

        ComboTD.setForeground(new java.awt.Color(255, 255, 255));
        ComboTD.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "Seleccione...", "Cedula de Ciudadania", "Cedula de Extranjeria", "Pasaporte", "Permiso Especial" }));

        txtEmail.setEditable(false);
        txtEmail.setForeground(new java.awt.Color(0, 0, 0));

        ComboPais.setForeground(new java.awt.Color(255, 255, 255));
        ComboPais.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "Seleccione...", "Colombia", "Estados Unidos" }));
        ComboPais.setToolTipText("");
        ComboPais.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                ComboPaisActionPerformed(evt);
            }
        });
        ComboPais.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                ComboPaisKeyPressed(evt);
            }
        });

        txtEstado.setEditable(false);
        txtEstado.setForeground(new java.awt.Color(0, 0, 0));
        txtEstado.setText("Activo");
        txtEstado.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txtEstadoActionPerformed(evt);
            }
        });

        btnGuardar.setIcon(new javax.swing.ImageIcon(getClass().getResource("/images/icons8-añadir-64.png"))); // NOI18N
        btnGuardar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnGuardarActionPerformed(evt);
            }
        });

        btnCancelar.setIcon(new javax.swing.ImageIcon(getClass().getResource("/images/icons8-cancelar-suscripción-40.png"))); // NOI18N
        btnCancelar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnCancelarActionPerformed(evt);
            }
        });

        ComboArea.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "Seleccione...", "Administracion", "Financiera", "Compras", "Infraestructura", "Operacion", "Talento Humano", "Servicios Varios" }));

        jLabel14.setForeground(new java.awt.Color(255, 255, 255));
        jLabel14.setText("Fecha y Hora de Registro");

        txtFechaRegistro.setEditable(false);

        txtPrimerApellido.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                txtPrimerApellidoKeyPressed(evt);
            }
            public void keyTyped(java.awt.event.KeyEvent evt) {
                txtPrimerApellidoKeyTyped(evt);
            }
        });

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addGap(25, 25, 25)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addComponent(jLabel2)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jLabel1)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(jButton1, javax.swing.GroupLayout.PREFERRED_SIZE, 42, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                        .addComponent(jSeparator1, javax.swing.GroupLayout.PREFERRED_SIZE, 646, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGroup(javax.swing.GroupLayout.Alignment.LEADING, jPanel1Layout.createSequentialGroup()
                            .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                .addGroup(jPanel1Layout.createSequentialGroup()
                                    .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                                        .addComponent(jLabel7)
                                        .addComponent(jLabel8)
                                        .addGroup(jPanel1Layout.createSequentialGroup()
                                            .addComponent(jLabel3)
                                            .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                            .addComponent(txtID, javax.swing.GroupLayout.PREFERRED_SIZE, 255, javax.swing.GroupLayout.PREFERRED_SIZE))
                                        .addGroup(jPanel1Layout.createSequentialGroup()
                                            .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                                .addComponent(jLabel5)
                                                .addComponent(jLabel6)
                                                .addComponent(jLabel4))
                                            .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                            .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                                .addComponent(ComboTD, 0, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                                .addGroup(jPanel1Layout.createSequentialGroup()
                                                    .addGap(6, 6, 6)
                                                    .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                                        .addComponent(txtPrimerNombre)
                                                        .addComponent(txtSegundoApellido)
                                                        .addComponent(txtOtrosNombres)
                                                        .addComponent(txtPrimerApellido))))))
                                    .addGap(18, 18, 18)
                                    .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                        .addComponent(jLabel9)
                                        .addComponent(jLabel12)
                                        .addComponent(jLabel11)
                                        .addComponent(jLabel10)
                                        .addComponent(jLabel13)
                                        .addComponent(jLabel14)))
                                .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel1Layout.createSequentialGroup()
                                    .addComponent(btnGuardar)
                                    .addGap(8, 8, 8)))
                            .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                                .addComponent(ComboPais, javax.swing.GroupLayout.Alignment.LEADING, 0, 356, Short.MAX_VALUE)
                                .addComponent(txtFechaIngreso, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                .addComponent(ComboArea, javax.swing.GroupLayout.PREFERRED_SIZE, 350, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addComponent(txtEstado, javax.swing.GroupLayout.Alignment.LEADING)
                                .addComponent(btnCancelar, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.PREFERRED_SIZE, 84, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addComponent(txtEmail)
                                .addComponent(txtFechaRegistro, javax.swing.GroupLayout.Alignment.LEADING)))))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jButton1, javax.swing.GroupLayout.PREFERRED_SIZE, 44, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel2, javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(jLabel1, javax.swing.GroupLayout.Alignment.TRAILING))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jSeparator1, javax.swing.GroupLayout.PREFERRED_SIZE, 15, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                            .addComponent(jLabel3)
                            .addComponent(txtID, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jLabel4)
                            .addComponent(ComboTD, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jLabel5)
                            .addComponent(jLabel11)
                            .addComponent(txtPrimerApellido, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jLabel9)
                            .addComponent(txtEmail, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jLabel10)
                            .addComponent(ComboPais, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(txtFechaIngreso, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jLabel6)
                            .addComponent(txtSegundoApellido, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jLabel12))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jLabel7)
                            .addComponent(txtPrimerNombre, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jLabel13)
                            .addComponent(txtEstado, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
                    .addComponent(ComboArea, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(18, 18, 18)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel8)
                    .addComponent(txtOtrosNombres, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel14)
                    .addComponent(txtFechaRegistro, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(btnCancelar, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(btnGuardar, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addGap(21, 21, 21))
        );

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents


    
    private void jButton1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton1ActionPerformed
       this.setVisible(false);
       Principal ob = new Principal();
       ob.setVisible(true);
    }//GEN-LAST:event_jButton1ActionPerformed

    private void txtIDKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtIDKeyPressed
        // TODO add your handling code here:
    }//GEN-LAST:event_txtIDKeyPressed

 
    //evento que permite validar que los inputs no permitan el ingreso de numeros
    
    private void txtSegundoApellidoKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtSegundoApellidoKeyPressed
        char validar=evt.getKeyChar();
        if(Character.isDigit(validar)){
            getToolkit().beep();
            evt.consume();
            JOptionPane.showMessageDialog(null,"Digite solamente letras");
            txtSegundoApellido.setText("");
        }
    }//GEN-LAST:event_txtSegundoApellidoKeyPressed

    //evento que permite definir el maximo de caracteres y la conversion de minusculas a mayusculas
    private void txtSegundoApellidoKeyTyped(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtSegundoApellidoKeyTyped
          if(txtSegundoApellido.getText().length() == 20){   
           evt.consume(); 
       }

        char c = evt.getKeyChar();
        if(Character.isLowerCase(c)){
            String cad = (""+c).toUpperCase();
            c= cad.charAt(0);
            evt.setKeyChar(c);
        }
    }//GEN-LAST:event_txtSegundoApellidoKeyTyped
    //evento que permite validar que los inputs no permitan el ingreso de numeros
    private void txtPrimerNombreKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtPrimerNombreKeyPressed
        char validar=evt.getKeyChar();
        if(Character.isDigit(validar)){
            getToolkit().beep();
            evt.consume();
            JOptionPane.showMessageDialog(null,"Digite solamente letras");
            txtPrimerNombre.setText("");
        }
    }//GEN-LAST:event_txtPrimerNombreKeyPressed

    
    //Evento que permite otorgarle dinamicamente el correo del empleado, la longitud del primer nombre y que la letra se convierta en mayuscula
    private void txtPrimerNombreKeyTyped(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtPrimerNombreKeyTyped
     int res=  registrar.CantidadEmpleados()+1;
        
    if(txtPrimerApellido.getText().equals("") || txtPrimerNombre.getText().equals("")) { //Si alguno de los dos este vacio...
            txtEmail.setText("");
     }else{ //Si ninguno este vacio
              if(ComboPais.getSelectedIndex() ==1) {    //Es decir si es colombia
        txtEmail.setText(txtPrimerNombre.getText().toLowerCase().toString().trim()+"."+txtPrimerApellido.getText().toLowerCase().toString().trim()+"."+res+"@cidenet.com.co".trim());
       }else if(ComboPais.getSelectedIndex() ==2) {    //Es decir si es estados unidso
                 txtEmail.setText(txtPrimerNombre.getText().toLowerCase().toString().trim()+"."+txtPrimerApellido.getText().toLowerCase().toString().trim()+"."+res+"@cidenet.com.us".trim());
     }
     }
      
        

        
        
        if(txtPrimerNombre.getText().length() == 20){   
           evt.consume(); 
     
       }


        char c = evt.getKeyChar();
        if(Character.isLowerCase(c)){
            String cad = (""+c).toUpperCase();
            c= cad.charAt(0);
            evt.setKeyChar(c);
        }
    }//GEN-LAST:event_txtPrimerNombreKeyTyped

        //evento que permite validar que los inputs no permitan el ingreso de numeros y indicando la cantidad de caracteres
    private void txtOtrosNombresKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtOtrosNombresKeyPressed
       if(txtOtrosNombres.getText().length() == 50){   
        evt.consume(); 
       }
        
        
        char validar=evt.getKeyChar();
        if(Character.isDigit(validar)){
            getToolkit().beep();
            evt.consume();
            JOptionPane.showMessageDialog(null,"Digite solamente letras");
            txtOtrosNombres.setText("");
        }
    }//GEN-LAST:event_txtOtrosNombresKeyPressed

    //evento que permite indicar que se convierta en mayuscula las letras y que no hayan espacios en el campo de otros nombres
    private void txtOtrosNombresKeyTyped(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtOtrosNombresKeyTyped
        txtOtrosNombres.setText(txtOtrosNombres.getText().trim());
        char c = evt.getKeyChar();
        if(Character.isLowerCase(c)){
            String cad = (""+c).toUpperCase();
            c= cad.charAt(0);
            evt.setKeyChar(c);
        }
    }//GEN-LAST:event_txtOtrosNombresKeyTyped

    private void txtEstadoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txtEstadoActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_txtEstadoActionPerformed

    //evento que permite guardar los registros en la BD y realizando un riguroso proceso de validaciones de datos
    private void btnGuardarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnGuardarActionPerformed

        if(ComboPais.getSelectedItem().equals("Seleccione..."))
        {
            JOptionPane.showMessageDialog(this,"Señor usuario seleccione el pais","Error",JOptionPane.ERROR_MESSAGE);
        }
        if( ComboTD.getSelectedItem().equals("Seleccione..."))
        {
            JOptionPane.showMessageDialog(this,"Señor usuario seleccione el tipo de identificacion ","Error",JOptionPane.ERROR_MESSAGE);
        }
        if( ComboArea.getSelectedItem().equals("Seleccione..."))
        {
            JOptionPane.showMessageDialog(this,"Señor usuario seleccione el area ","Error",JOptionPane.ERROR_MESSAGE);
        }

        if(txtEmail.getText().equals("") || txtEstado.getText().equals("") || txtID.getText().equals("")  || txtPrimerApellido.getText().equals("") || txtPrimerNombre.getText().equals("") ||
            txtSegundoApellido.getText().equals("") || ComboPais.getSelectedItem().equals("Seleccione...")  ||
            ComboTD.getSelectedItem().equals("Seleccione...")  || ComboArea.getSelectedItem().equals("Seleccione...") || txtFechaIngreso.getDate()==null){
            JOptionPane.showMessageDialog(this,"Señor usuario Verifique Que Todo El Formulario Este Completamente Diligenciado","Error",JOptionPane.ERROR_MESSAGE);
        }else{

            //Fecha del input introducido
             java.util.Date fechaInput=txtFechaIngreso.getDate();
            String CorreoElectronico = txtEmail.getText();
            String Estado= txtEstado.getText();
            String ID= txtID.getText();
            String OtrosNombres = txtOtrosNombres.getText();
            String PrimerApellido = txtPrimerApellido.getText();
            String PrimerNombre= txtPrimerNombre.getText();
            String SegundoApellido =  txtSegundoApellido.getText();
            String Pais =   ComboPais.getSelectedItem().toString();
            String TipodeDocumento =  ComboTD.getSelectedItem().toString();
            String Area =  ComboArea.getSelectedItem().toString();
           
            //Obtenemos fechas del sistema y obtenemos el dato obtenido del JDateChooser y lo convertirmos en dato valido para la BD
            java.sql.Date fechaIngreso = new java.sql.Date(fechaInput.getTime());
            Timestamp fechaRegistro = new Timestamp(System.currentTimeMillis());
Timestamp fechaedicion = new Timestamp(System.currentTimeMillis());

            //Validamos si existe un usuario con el numer de identificacion y tipo de identificacion
            int ValidacionUsuario = registrar.ValidarEmpleado(ID,TipodeDocumento);
            
            //Si existe un usuario con el id y tipo de documento suministrados en el metodo, no le va permitir registrarlo y mostrara un mensaje de error...
            if(ValidacionUsuario >0){
                JOptionPane.showMessageDialog(null,"Señor usuario el id y tipo de documento suministrados, ya un empleado los tiene, por favor ingrese nuevamente datos correctos!");
            }
            //En el caso contrario si no existe un usuario con el id y tipo de documento suministrados, le va permitir registrarlo..
            else{
                          Employee emp = new Employee(ID,TipodeDocumento,PrimerApellido,SegundoApellido, PrimerNombre,OtrosNombres,CorreoElectronico,Pais,fechaIngreso,Area,Estado,fechaRegistro,fechaedicion);
            int res=  registrar.GuardarEmpleao(emp);

            if(res >0){

                Principal ob = new Principal();
                this.setVisible(false);
                ob.setVisible(true);

                JOptionPane.showMessageDialog(null,"Registro Exitosos");
                txtEmail.setText("");
                txtEstado.setText("");
                txtID.setText("");
                txtOtrosNombres.setText("");
                txtPrimerApellido.setText("");
                txtPrimerNombre.setText("");
                txtSegundoApellido.setText("");
                ComboPais.setSelectedItem("Seleccione...");
                ComboTD.setSelectedItem("Seleccione...");
                ComboArea.setSelectedItem("Seleccione...");
                txtFechaIngreso.setCalendar(null);

            }else{
                JOptionPane.showMessageDialog(null,"Hubo un error en el registro!");
            }  
            }
        }

    }//GEN-LAST:event_btnGuardarActionPerformed

    //Metodo que permite obtener la fecha un mes hacia atraz, esto de acuerdo con la fecha del sistema...
    public java.util.Date RestarMes(java.util.Date fecha, int meses){
        Calendar ca = Calendar.getInstance(); 
        ca.setTime(fecha);
        ca.add(Calendar.MONTH, meses);
        return ca.getTime();
    }
    
    //evento de limpiar campos
    private void btnCancelarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnCancelarActionPerformed
        txtEmail.setText("");
        txtEstado.setText("");
        txtID.setText("");
        txtOtrosNombres.setText("");
        txtPrimerApellido.setText("");
        txtPrimerNombre.setText("");
        txtSegundoApellido.setText("");
        ComboPais.setSelectedItem("Seleccione...");
        ComboTD.setSelectedItem("Seleccione...");
        ComboArea.setSelectedItem("Seleccione...");
        txtFechaIngreso.setCalendar(null);
    }//GEN-LAST:event_btnCancelarActionPerformed

    //Evento que indicamos la cantidad maxima de caracteres en el id
    private void txtIDKeyTyped(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtIDKeyTyped
       if(txtID.getText().length() == 20){   
           evt.consume();
           
       }
    }//GEN-LAST:event_txtIDKeyTyped

    private void ComboPaisKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_ComboPaisKeyPressed
        // TODO add your handling code here:
    }//GEN-LAST:event_ComboPaisKeyPressed

    //Evento que permite cambiar el dominio del correo electronico, esto dinamicamente....
    private void ComboPaisActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_ComboPaisActionPerformed
       paisSeleccionado = ComboPais.getSelectedItem().toString();
            int res=  registrar.CantidadEmpleados()+1;
            
            if(txtPrimerApellido.getText().equals("") || txtPrimerNombre.getText().equals("")) { //Si alguno de los dos este vacio...
         txtEmail.setText("");
     }else{ //Si ninguno este vacio
       if(paisSeleccionado.equals("Colombia")){  
                   txtEmail.setText(txtPrimerNombre.getText().toLowerCase().toString().trim()+"."+txtPrimerApellido.getText().toLowerCase().toString().trim()+"."+res+"@cidenet.com.co".trim());
       }else if(paisSeleccionado.equals("Estados Unidos")){  
                   txtEmail.setText(txtPrimerNombre.getText().toLowerCase().toString().trim()+"."+txtPrimerApellido.getText().toLowerCase().toString().trim()+"."+res+"@cidenet.com.us".trim());
       }else{ 
           txtEmail.setText("");
           JOptionPane.showMessageDialog(null,"Señor usuario seleccione un pais!");
       }
     }
       
       

    }//GEN-LAST:event_ComboPaisActionPerformed

    private void jPanel1KeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_jPanel1KeyReleased
        // TODO add your handling code here:
    }//GEN-LAST:event_jPanel1KeyReleased

        //Metodo que permite validar que los inputs no permitan el ingreso de numeros
    private void txtPrimerApellidoKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtPrimerApellidoKeyPressed
             char validar=evt.getKeyChar();
        if(Character.isDigit(validar)){
            getToolkit().beep();
            evt.consume();
            JOptionPane.showMessageDialog(null,"Digite solamente letras");
            txtPrimerApellido.setText("");
        }
    }//GEN-LAST:event_txtPrimerApellidoKeyPressed

    private void txtPrimerApellidoKeyTyped(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtPrimerApellidoKeyTyped
        int res=  registrar.CantidadEmpleados()+1;
        
    if(txtPrimerApellido.getText().equals("") || txtPrimerNombre.getText().equals("")) { //Si alguno de los dos este vacio...
            txtEmail.setText("");
     }else{ //Si ninguno este vacio
              if(ComboPais.getSelectedIndex() ==1) {    //Es decir si es colombia
        txtEmail.setText(txtPrimerNombre.getText().toLowerCase().toString().trim()+"."+txtPrimerApellido.getText().toLowerCase().toString().trim()+"."+res+"@cidenet.com.co".trim());
       }else if(ComboPais.getSelectedIndex() ==2) {    //Es decir si es estados unidso
                 txtEmail.setText(txtPrimerNombre.getText().toLowerCase().toString().trim()+"."+txtPrimerApellido.getText().toLowerCase().toString().trim()+"."+res+"@cidenet.com.us".trim());
     }
     }
      
       
        if(txtPrimerApellido.getText().length() == 20){   
           evt.consume(); 
     
       }


        char c = evt.getKeyChar();
        if(Character.isLowerCase(c)){
            String cad = (""+c).toUpperCase();
            c= cad.charAt(0);
            evt.setKeyChar(c);
        }
    }//GEN-LAST:event_txtPrimerApellidoKeyTyped

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(NuevoEmpleado.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(NuevoEmpleado.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(NuevoEmpleado.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(NuevoEmpleado.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {

                new NuevoEmpleado().setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JComboBox<String> ComboArea;
    private javax.swing.JComboBox<String> ComboPais;
    private javax.swing.JComboBox<String> ComboTD;
    private javax.swing.JToggleButton btnCancelar;
    private javax.swing.JToggleButton btnGuardar;
    private javax.swing.JButton jButton1;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel10;
    private javax.swing.JLabel jLabel11;
    private javax.swing.JLabel jLabel12;
    private javax.swing.JLabel jLabel13;
    private javax.swing.JLabel jLabel14;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JLabel jLabel7;
    private javax.swing.JLabel jLabel8;
    private javax.swing.JLabel jLabel9;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JSeparator jSeparator1;
    private javax.swing.JTextField txtEmail;
    private javax.swing.JTextField txtEstado;
    private com.toedter.calendar.JDateChooser txtFechaIngreso;
    private javax.swing.JTextField txtFechaRegistro;
    private javax.swing.JTextField txtID;
    private javax.swing.JTextField txtOtrosNombres;
    private javax.swing.JTextField txtPrimerApellido;
    private javax.swing.JTextField txtPrimerNombre;
    private javax.swing.JTextField txtSegundoApellido;
    // End of variables declaration//GEN-END:variables
}
