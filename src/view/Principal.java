package view;

import controller.ControllerEmployee;
import java.awt.Point;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.logging.Level;
import java.util.logging.Logger;
import utilities.ModeloDeTabla;
import javax.swing.JComboBox;
import javax.swing.JOptionPane;
import javax.swing.JTable;
import javax.swing.RowFilter;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableRowSorter;
import model.Employee;
import model.EmployeeDAO;

public class Principal extends javax.swing.JFrame {
 
    //Inicalizamos variables para trabajar con la gestion de empledos
    EmployeeDAO opc;
      private TableRowSorter trsfiltro;
    String filtro;
    String IdentificacionEmpleado="";
  String NombresApellidos;
  //Indicamos que el combobox va permitir unicamente valores enteros
    public JComboBox<Integer> pageJComboBox;
//llamaremos al controlador de empleados
    private final ControllerEmployee controlador;
    //inicializamos un defaultablemodel para trabajar con la parte de eliminar filas
    public DefaultTableModel modelo;

    public Principal() {
        initComponents();
        modelo = (DefaultTableModel) tablaEmpleados.getModel();
       opc = new EmployeeDAO();
         this.setLocationRelativeTo(null); 
        this.tablaEmpleados.setModel(crearModeloDeTabla());
        controlador = new ControllerEmployee(this);
        
        //Evento de tipo MouseListener, permitiendo obtener los valores de cada columna y ponerlos en los JTextfield de la parte de abajo de la tabla
        tablaEmpleados.addMouseListener(new MouseAdapter(){
        public void mousePressed(MouseEvent evt){
            JTable table = (JTable) evt.getSource();
            Point point = evt.getPoint();
            int row = table.rowAtPoint(point);
            if (evt.getClickCount() ==1){
                
                IdentificacionEmpleado = tablaEmpleados.getValueAt(tablaEmpleados.getSelectedRow(), 0).toString();
                NombresApellidos = tablaEmpleados.getValueAt(tablaEmpleados.getSelectedRow(), 2).toString()+" "+tablaEmpleados.getValueAt(tablaEmpleados.getSelectedRow(), 3).toString()
                        + " "+tablaEmpleados.getValueAt(tablaEmpleados.getSelectedRow(), 4).toString()+" "+tablaEmpleados.getValueAt(tablaEmpleados.getSelectedRow(), 5).toString();
                
                txtID.setText(tablaEmpleados.getValueAt(tablaEmpleados.getSelectedRow(), 0).toString());
                txtTD.setText(tablaEmpleados.getValueAt(tablaEmpleados.getSelectedRow(), 1).toString());
                txtPrimerApellido.setText(tablaEmpleados.getValueAt(tablaEmpleados.getSelectedRow(), 2).toString());
                txtSegundoApellido.setText(tablaEmpleados.getValueAt(tablaEmpleados.getSelectedRow(), 3).toString());  
                txtPrimerNombre.setText(tablaEmpleados.getValueAt(tablaEmpleados.getSelectedRow(), 4).toString()); 
                txtOtrosNombres.setText(tablaEmpleados.getValueAt(tablaEmpleados.getSelectedRow(), 5).toString()); 
                txtEmail.setText(tablaEmpleados.getValueAt(tablaEmpleados.getSelectedRow(), 6).toString());
                txtPais.setText(tablaEmpleados.getValueAt(tablaEmpleados.getSelectedRow(), 7).toString());
                txtFechaIngreso.setText(tablaEmpleados.getValueAt(tablaEmpleados.getSelectedRow(), 8).toString());
                txtArea.setText(tablaEmpleados.getValueAt(tablaEmpleados.getSelectedRow(), 9).toString());
                txtEstado.setText(tablaEmpleados.getValueAt(tablaEmpleados.getSelectedRow(), 10).toString());
                txtFechaRegistro.setText(tablaEmpleados.getValueAt(tablaEmpleados.getSelectedRow(), 11).toString());
                txtEdicion.setText(tablaEmpleados.getValueAt(tablaEmpleados.getSelectedRow(), 12).toString());
            }
        }
        });
        
        
    }
    
    
    //Metodo y variable que nos va permitir filtrar por diferentes filtros...
        int ColumnaSeleccionada;
        
        public void filtro() {
            
        if(radiobtnIdentificacion.isSelected()){
            ColumnaSeleccionada=0;
        
          
        }else if(radiobtnTipoDocumento.isSelected()){
            ColumnaSeleccionada=1;
        
           
        }else if(radiobtnPrimerApellido.isSelected()){
            ColumnaSeleccionada=2;
            
         
        }else if(radiobtnSegundoApellido.isSelected()){
            ColumnaSeleccionada=3;
          
           
        }else if(radiobtnPrimerNombre.isSelected()){
            ColumnaSeleccionada=4;
         
        }else if(radiobtnOtrosNombres.isSelected()){
            ColumnaSeleccionada=5;
             
           
        }else if(radiobtnCorreo.isSelected()){
            ColumnaSeleccionada=6;
           
           
        }else if(radiobtnPais.isSelected()){
            ColumnaSeleccionada=7;
            
          
        }else if(radiobtnEstado.isSelected()){
            ColumnaSeleccionada=10;
          
        }
        
        filtro = txtBusqueda.getText();
        trsfiltro.setRowFilter(RowFilter.regexFilter(txtBusqueda.getText(), ColumnaSeleccionada));
    }
        

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        buttonGroup1 = new javax.swing.ButtonGroup();
        panelPrincipal = new javax.swing.JPanel();
        panelConsulta = new javax.swing.JPanel();
        paginationPanel = new javax.swing.JPanel();
        tableSroll = new javax.swing.JScrollPane();
        tablaEmpleados = new javax.swing.JTable();
        jLabel7 = new javax.swing.JLabel();
        txtBusqueda = new javax.swing.JTextField();
        btnAgregar = new javax.swing.JButton();
        btnCerrar = new javax.swing.JButton();
        btnMinimizar = new javax.swing.JButton();
        btnEditar = new javax.swing.JButton();
        btnEliminar = new javax.swing.JButton();
        jLabel1 = new javax.swing.JLabel();
        jLabel2 = new javax.swing.JLabel();
        jLabel3 = new javax.swing.JLabel();
        jLabel4 = new javax.swing.JLabel();
        jLabel5 = new javax.swing.JLabel();
        jLabel6 = new javax.swing.JLabel();
        jLabel8 = new javax.swing.JLabel();
        jLabel9 = new javax.swing.JLabel();
        jLabel10 = new javax.swing.JLabel();
        jLabel11 = new javax.swing.JLabel();
        jLabel12 = new javax.swing.JLabel();
        jLabel13 = new javax.swing.JLabel();
        txtID = new javax.swing.JTextField();
        txtTD = new javax.swing.JTextField();
        txtPrimerApellido = new javax.swing.JTextField();
        txtSegundoApellido = new javax.swing.JTextField();
        txtPrimerNombre = new javax.swing.JTextField();
        txtPais = new javax.swing.JTextField();
        txtEmail = new javax.swing.JTextField();
        txtFechaRegistro = new javax.swing.JTextField();
        txtFechaIngreso = new javax.swing.JTextField();
        txtArea = new javax.swing.JTextField();
        txtEstado = new javax.swing.JTextField();
        txtOtrosNombres = new javax.swing.JTextField();
        jLabel14 = new javax.swing.JLabel();
        jSeparator1 = new javax.swing.JSeparator();
        jLabel15 = new javax.swing.JLabel();
        radiobtnOtrosNombres = new javax.swing.JRadioButton();
        radiobtnPrimerApellido = new javax.swing.JRadioButton();
        radiobtnSegundoApellido = new javax.swing.JRadioButton();
        radiobtnTipoDocumento = new javax.swing.JRadioButton();
        radiobtnIdentificacion = new javax.swing.JRadioButton();
        radiobtnPais = new javax.swing.JRadioButton();
        radiobtnCorreo = new javax.swing.JRadioButton();
        radiobtnEstado = new javax.swing.JRadioButton();
        radiobtnPrimerNombre = new javax.swing.JRadioButton();
        jButton1 = new javax.swing.JButton();
        txtEdicion = new javax.swing.JTextField();
        jLabel16 = new javax.swing.JLabel();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        setUndecorated(true);

        panelPrincipal.setBackground(java.awt.SystemColor.activeCaption);
        panelPrincipal.setBorder(new javax.swing.border.SoftBevelBorder(javax.swing.border.BevelBorder.RAISED));

        panelConsulta.setBackground(java.awt.SystemColor.activeCaption);

        paginationPanel.setBackground(java.awt.SystemColor.activeCaption);
        paginationPanel.setBorder(javax.swing.BorderFactory.createBevelBorder(javax.swing.border.BevelBorder.RAISED));
        paginationPanel.setForeground(java.awt.Color.white);

        tableSroll.setPreferredSize(new java.awt.Dimension(683, 280));

        tablaEmpleados.setBorder(javax.swing.BorderFactory.createBevelBorder(javax.swing.border.BevelBorder.RAISED));
        tablaEmpleados.setForeground(java.awt.SystemColor.activeCaptionText);
        tablaEmpleados.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {

            }
        ));
        tableSroll.setViewportView(tablaEmpleados);

        jLabel7.setFont(new java.awt.Font("Dialog", 1, 36)); // NOI18N
        jLabel7.setForeground(java.awt.Color.white);
        jLabel7.setText("Listado de Empleados");

        txtBusqueda.setBackground(new java.awt.Color(102, 102, 255));
        txtBusqueda.setForeground(new java.awt.Color(255, 255, 255));
        txtBusqueda.setToolTipText("");
        txtBusqueda.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyReleased(java.awt.event.KeyEvent evt) {
                txtBusquedaKeyReleased(evt);
            }
        });

        btnAgregar.setBackground(javax.swing.UIManager.getDefaults().getColor("Button.darcula.selection.color2"));
        btnAgregar.setIcon(new javax.swing.ImageIcon(getClass().getResource("/images/icons8-añadir-grupo-de-usuarios-hombre-hombre-64.png"))); // NOI18N
        btnAgregar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnAgregarActionPerformed(evt);
            }
        });

        btnCerrar.setIcon(new javax.swing.ImageIcon(getClass().getResource("/images/icons8-cerrar-ventana-80 (1).png"))); // NOI18N
        btnCerrar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnCerrarActionPerformed(evt);
            }
        });

        btnMinimizar.setIcon(new javax.swing.ImageIcon(getClass().getResource("/images/icons8-minimizar-la-ventana-48_1.png"))); // NOI18N
        btnMinimizar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnMinimizarActionPerformed(evt);
            }
        });

        btnEditar.setBackground(javax.swing.UIManager.getDefaults().getColor("Button.darcula.selection.color1"));
        btnEditar.setIcon(new javax.swing.ImageIcon(getClass().getResource("/images/icons8-editar-propiedad-40.png"))); // NOI18N
        btnEditar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnEditarActionPerformed(evt);
            }
        });

        btnEliminar.setBackground(javax.swing.UIManager.getDefaults().getColor("Button.darcula.selection.color2"));
        btnEliminar.setIcon(new javax.swing.ImageIcon(getClass().getResource("/images/icons8-eliminar-40.png"))); // NOI18N
        btnEliminar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnEliminarActionPerformed(evt);
            }
        });

        jLabel1.setForeground(new java.awt.Color(255, 255, 255));
        jLabel1.setText("Identificacion");

        jLabel2.setForeground(new java.awt.Color(255, 255, 255));
        jLabel2.setText("Tipo de Documento");

        jLabel3.setForeground(new java.awt.Color(255, 255, 255));
        jLabel3.setText("Primer Apellido");

        jLabel4.setForeground(new java.awt.Color(255, 255, 255));
        jLabel4.setText("Segundo Apellido");

        jLabel5.setForeground(new java.awt.Color(255, 255, 255));
        jLabel5.setText("Primer Nombre");

        jLabel6.setForeground(new java.awt.Color(255, 255, 255));
        jLabel6.setText("Otros Nombres");

        jLabel8.setForeground(new java.awt.Color(255, 255, 255));
        jLabel8.setText("Correo Electronico");

        jLabel9.setForeground(new java.awt.Color(255, 255, 255));
        jLabel9.setText("Pais Empleo");

        jLabel10.setForeground(new java.awt.Color(255, 255, 255));
        jLabel10.setText("Fecha de Ingreso");

        jLabel11.setForeground(new java.awt.Color(255, 255, 255));
        jLabel11.setText("Area");

        jLabel12.setForeground(new java.awt.Color(255, 255, 255));
        jLabel12.setText("Estado");

        jLabel13.setForeground(new java.awt.Color(255, 255, 255));
        jLabel13.setText("Fecha de Registro");

        txtID.setEditable(false);
        txtID.setBackground(new java.awt.Color(102, 102, 255));
        txtID.setForeground(new java.awt.Color(255, 255, 255));

        txtTD.setEditable(false);
        txtTD.setBackground(new java.awt.Color(102, 102, 255));
        txtTD.setForeground(new java.awt.Color(255, 255, 255));

        txtPrimerApellido.setEditable(false);
        txtPrimerApellido.setBackground(new java.awt.Color(102, 102, 255));
        txtPrimerApellido.setForeground(new java.awt.Color(255, 255, 255));

        txtSegundoApellido.setEditable(false);
        txtSegundoApellido.setBackground(new java.awt.Color(102, 102, 255));
        txtSegundoApellido.setForeground(new java.awt.Color(255, 255, 255));

        txtPrimerNombre.setEditable(false);
        txtPrimerNombre.setBackground(new java.awt.Color(102, 102, 255));
        txtPrimerNombre.setForeground(new java.awt.Color(255, 255, 255));

        txtPais.setEditable(false);
        txtPais.setBackground(new java.awt.Color(102, 102, 255));
        txtPais.setForeground(new java.awt.Color(255, 255, 255));

        txtEmail.setEditable(false);
        txtEmail.setBackground(new java.awt.Color(102, 102, 255));
        txtEmail.setForeground(new java.awt.Color(255, 255, 255));

        txtFechaRegistro.setEditable(false);
        txtFechaRegistro.setBackground(new java.awt.Color(102, 102, 255));
        txtFechaRegistro.setForeground(new java.awt.Color(255, 255, 255));

        txtFechaIngreso.setEditable(false);
        txtFechaIngreso.setBackground(new java.awt.Color(102, 102, 255));
        txtFechaIngreso.setForeground(new java.awt.Color(255, 255, 255));

        txtArea.setEditable(false);
        txtArea.setBackground(new java.awt.Color(102, 102, 255));
        txtArea.setForeground(new java.awt.Color(255, 255, 255));

        txtEstado.setEditable(false);
        txtEstado.setBackground(new java.awt.Color(102, 102, 255));
        txtEstado.setForeground(new java.awt.Color(255, 255, 255));

        txtOtrosNombres.setEditable(false);
        txtOtrosNombres.setBackground(new java.awt.Color(102, 102, 255));
        txtOtrosNombres.setForeground(new java.awt.Color(255, 255, 255));

        jLabel14.setIcon(new javax.swing.ImageIcon(getClass().getResource("/images/icons8-lista-64.png"))); // NOI18N

        jSeparator1.setBackground(new java.awt.Color(255, 255, 255));

        jLabel15.setForeground(new java.awt.Color(255, 255, 255));
        jLabel15.setText("Criterios de Busqueda");

        buttonGroup1.add(radiobtnOtrosNombres);
        radiobtnOtrosNombres.setForeground(new java.awt.Color(255, 255, 255));
        radiobtnOtrosNombres.setText("Otros Nombres");

        buttonGroup1.add(radiobtnPrimerApellido);
        radiobtnPrimerApellido.setForeground(new java.awt.Color(255, 255, 255));
        radiobtnPrimerApellido.setText("Primer Apellido");

        buttonGroup1.add(radiobtnSegundoApellido);
        radiobtnSegundoApellido.setForeground(new java.awt.Color(255, 255, 255));
        radiobtnSegundoApellido.setText("Segundo Apellido");

        buttonGroup1.add(radiobtnTipoDocumento);
        radiobtnTipoDocumento.setForeground(new java.awt.Color(255, 255, 255));
        radiobtnTipoDocumento.setText("Tipo de Documento");

        buttonGroup1.add(radiobtnIdentificacion);
        radiobtnIdentificacion.setForeground(new java.awt.Color(255, 255, 255));
        radiobtnIdentificacion.setSelected(true);
        radiobtnIdentificacion.setText("Identificacion");

        buttonGroup1.add(radiobtnPais);
        radiobtnPais.setForeground(new java.awt.Color(255, 255, 255));
        radiobtnPais.setText("Pais");

        buttonGroup1.add(radiobtnCorreo);
        radiobtnCorreo.setForeground(new java.awt.Color(255, 255, 255));
        radiobtnCorreo.setText("Correo Electronico");

        buttonGroup1.add(radiobtnEstado);
        radiobtnEstado.setForeground(new java.awt.Color(255, 255, 255));
        radiobtnEstado.setText("Estado");

        buttonGroup1.add(radiobtnPrimerNombre);
        radiobtnPrimerNombre.setForeground(new java.awt.Color(255, 255, 255));
        radiobtnPrimerNombre.setText("Primer Nombre");
        radiobtnPrimerNombre.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                radiobtnPrimerNombreActionPerformed(evt);
            }
        });

        jButton1.setIcon(new javax.swing.ImageIcon(getClass().getResource("/images/icons8-búsqueda-40.png"))); // NOI18N
        jButton1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton1ActionPerformed(evt);
            }
        });

        txtEdicion.setEditable(false);
        txtEdicion.setBackground(new java.awt.Color(102, 102, 255));
        txtEdicion.setForeground(new java.awt.Color(255, 255, 255));

        jLabel16.setForeground(new java.awt.Color(255, 255, 255));
        jLabel16.setText("Ultima edicion");

        javax.swing.GroupLayout panelPrincipalLayout = new javax.swing.GroupLayout(panelPrincipal);
        panelPrincipal.setLayout(panelPrincipalLayout);
        panelPrincipalLayout.setHorizontalGroup(
            panelPrincipalLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
            .addGroup(javax.swing.GroupLayout.Alignment.LEADING, panelPrincipalLayout.createSequentialGroup()
                .addGroup(panelPrincipalLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(panelPrincipalLayout.createSequentialGroup()
                        .addGap(12, 12, 12)
                        .addGroup(panelPrincipalLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jLabel2)
                            .addComponent(jLabel3)
                            .addComponent(jLabel1)
                            .addComponent(jLabel4))
                        .addGap(18, 18, 18)
                        .addGroup(panelPrincipalLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                            .addComponent(txtSegundoApellido, javax.swing.GroupLayout.DEFAULT_SIZE, 257, Short.MAX_VALUE)
                            .addComponent(txtID)
                            .addComponent(txtTD)
                            .addComponent(txtPrimerApellido))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addGroup(panelPrincipalLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jLabel5)
                            .addComponent(jLabel8)
                            .addComponent(jLabel9)
                            .addComponent(jLabel6))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(panelPrincipalLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(panelPrincipalLayout.createSequentialGroup()
                                .addGroup(panelPrincipalLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                                    .addComponent(txtPrimerNombre)
                                    .addComponent(txtOtrosNombres, javax.swing.GroupLayout.DEFAULT_SIZE, 243, Short.MAX_VALUE)
                                    .addComponent(txtEmail))
                                .addGap(18, 18, 18)
                                .addGroup(panelPrincipalLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                                    .addGroup(panelPrincipalLayout.createSequentialGroup()
                                        .addComponent(jLabel13)
                                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                        .addComponent(txtFechaRegistro, javax.swing.GroupLayout.PREFERRED_SIZE, 185, javax.swing.GroupLayout.PREFERRED_SIZE))
                                    .addGroup(panelPrincipalLayout.createSequentialGroup()
                                        .addComponent(jLabel10)
                                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                        .addComponent(txtFechaIngreso))
                                    .addGroup(panelPrincipalLayout.createSequentialGroup()
                                        .addComponent(jLabel11)
                                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                        .addComponent(txtArea, javax.swing.GroupLayout.PREFERRED_SIZE, 274, javax.swing.GroupLayout.PREFERRED_SIZE))))
                            .addGroup(panelPrincipalLayout.createSequentialGroup()
                                .addComponent(txtPais, javax.swing.GroupLayout.PREFERRED_SIZE, 190, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(jLabel12)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(txtEstado, javax.swing.GroupLayout.PREFERRED_SIZE, 123, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(jLabel16)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(txtEdicion, javax.swing.GroupLayout.PREFERRED_SIZE, 163, javax.swing.GroupLayout.PREFERRED_SIZE))))
                    .addGroup(panelPrincipalLayout.createSequentialGroup()
                        .addContainerGap()
                        .addGroup(panelPrincipalLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                            .addGroup(javax.swing.GroupLayout.Alignment.LEADING, panelPrincipalLayout.createSequentialGroup()
                                .addGroup(panelPrincipalLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                                    .addGroup(panelPrincipalLayout.createSequentialGroup()
                                        .addComponent(radiobtnIdentificacion)
                                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                        .addComponent(radiobtnTipoDocumento)
                                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                        .addComponent(radiobtnPrimerApellido)
                                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                        .addComponent(radiobtnSegundoApellido)
                                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                        .addComponent(radiobtnPrimerNombre)
                                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                        .addComponent(txtBusqueda, javax.swing.GroupLayout.PREFERRED_SIZE, 318, javax.swing.GroupLayout.PREFERRED_SIZE))
                                    .addGroup(panelPrincipalLayout.createSequentialGroup()
                                        .addComponent(radiobtnPais, javax.swing.GroupLayout.PREFERRED_SIZE, 94, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                        .addComponent(radiobtnCorreo)
                                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                        .addComponent(radiobtnEstado)
                                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                        .addComponent(radiobtnOtrosNombres))
                                    .addComponent(tableSroll, javax.swing.GroupLayout.PREFERRED_SIZE, 1173, javax.swing.GroupLayout.PREFERRED_SIZE))
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addGroup(panelPrincipalLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                                    .addComponent(btnAgregar, javax.swing.GroupLayout.PREFERRED_SIZE, 61, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addComponent(btnEditar, javax.swing.GroupLayout.PREFERRED_SIZE, 0, Short.MAX_VALUE)
                                    .addComponent(btnEliminar, javax.swing.GroupLayout.PREFERRED_SIZE, 0, Short.MAX_VALUE)
                                    .addComponent(jButton1, javax.swing.GroupLayout.PREFERRED_SIZE, 50, javax.swing.GroupLayout.PREFERRED_SIZE)))
                            .addComponent(paginationPanel, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.PREFERRED_SIZE, 1173, javax.swing.GroupLayout.PREFERRED_SIZE))))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
            .addGroup(javax.swing.GroupLayout.Alignment.LEADING, panelPrincipalLayout.createSequentialGroup()
                .addGroup(panelPrincipalLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(panelPrincipalLayout.createSequentialGroup()
                        .addComponent(jLabel14)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(panelPrincipalLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(panelPrincipalLayout.createSequentialGroup()
                                .addComponent(jLabel7)
                                .addGap(634, 634, 634)
                                .addComponent(btnMinimizar, javax.swing.GroupLayout.PREFERRED_SIZE, 37, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(btnCerrar, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addComponent(jSeparator1, javax.swing.GroupLayout.PREFERRED_SIZE, 1174, javax.swing.GroupLayout.PREFERRED_SIZE)))
                    .addGroup(panelPrincipalLayout.createSequentialGroup()
                        .addContainerGap()
                        .addComponent(jLabel15)))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(panelConsulta, javax.swing.GroupLayout.PREFERRED_SIZE, 962, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap())
        );
        panelPrincipalLayout.setVerticalGroup(
            panelPrincipalLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(panelPrincipalLayout.createSequentialGroup()
                .addGroup(panelPrincipalLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addGroup(panelPrincipalLayout.createSequentialGroup()
                        .addGroup(panelPrincipalLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jLabel14)
                            .addGroup(panelPrincipalLayout.createSequentialGroup()
                                .addGroup(panelPrincipalLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(jLabel7)
                                    .addComponent(btnMinimizar, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addComponent(btnCerrar, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE))
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(jSeparator1, javax.swing.GroupLayout.PREFERRED_SIZE, 10, javax.swing.GroupLayout.PREFERRED_SIZE)))
                        .addGap(4, 4, 4)
                        .addComponent(jLabel15))
                    .addComponent(panelConsulta, javax.swing.GroupLayout.PREFERRED_SIZE, 18, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(6, 6, 6)
                .addGroup(panelPrincipalLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(jButton1, javax.swing.GroupLayout.PREFERRED_SIZE, 41, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addGroup(panelPrincipalLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(radiobtnPrimerApellido)
                        .addComponent(radiobtnSegundoApellido)
                        .addComponent(radiobtnTipoDocumento)
                        .addComponent(radiobtnIdentificacion)
                        .addComponent(radiobtnPrimerNombre)
                        .addComponent(txtBusqueda, javax.swing.GroupLayout.PREFERRED_SIZE, 41, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addGap(2, 2, 2)
                .addGroup(panelPrincipalLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(radiobtnPais)
                    .addComponent(radiobtnCorreo)
                    .addComponent(radiobtnEstado)
                    .addComponent(radiobtnOtrosNombres))
                .addGap(31, 31, 31)
                .addGroup(panelPrincipalLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                    .addGroup(panelPrincipalLayout.createSequentialGroup()
                        .addComponent(btnAgregar, javax.swing.GroupLayout.PREFERRED_SIZE, 62, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(btnEditar)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(btnEliminar))
                    .addComponent(tableSroll, javax.swing.GroupLayout.PREFERRED_SIZE, 0, Short.MAX_VALUE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(paginationPanel, javax.swing.GroupLayout.PREFERRED_SIZE, 42, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGroup(panelPrincipalLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(panelPrincipalLayout.createSequentialGroup()
                        .addGap(18, 18, 18)
                        .addGroup(panelPrincipalLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(panelPrincipalLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                .addComponent(jLabel1)
                                .addComponent(txtID, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addGroup(panelPrincipalLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                .addComponent(jLabel5)
                                .addComponent(txtPrimerNombre, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addGroup(panelPrincipalLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                .addComponent(jLabel13)
                                .addComponent(txtFechaRegistro, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
                        .addGap(10, 10, 10)
                        .addComponent(txtFechaIngreso, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(panelPrincipalLayout.createSequentialGroup()
                        .addGap(53, 53, 53)
                        .addGroup(panelPrincipalLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jLabel2)
                            .addComponent(txtTD, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jLabel6)
                            .addComponent(txtOtrosNombres, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jLabel10))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addGroup(panelPrincipalLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jLabel3)
                            .addComponent(txtPrimerApellido, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jLabel8)
                            .addComponent(txtEmail, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jLabel11)
                            .addComponent(txtArea, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(panelPrincipalLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jLabel4)
                    .addGroup(panelPrincipalLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(txtSegundoApellido, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addComponent(jLabel9))
                    .addGroup(panelPrincipalLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(jLabel12)
                        .addComponent(txtEstado, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addComponent(jLabel16)
                        .addComponent(txtEdicion, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addComponent(txtPais, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap(26, Short.MAX_VALUE))
        );

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addComponent(panelPrincipal, javax.swing.GroupLayout.PREFERRED_SIZE, 1257, Short.MAX_VALUE)
                .addContainerGap())
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addComponent(panelPrincipal, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(0, 0, Short.MAX_VALUE))
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void btnCerrarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnCerrarActionPerformed
                 int opcion=JOptionPane.showConfirmDialog(null,"¿Estas seguro de Cerrar el Sistema?");
        if(opcion==0){
        System.exit(0);   
        }
    }//GEN-LAST:event_btnCerrarActionPerformed

    private void btnMinimizarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnMinimizarActionPerformed
         this.setExtendedState(ICONIFIED);
    }//GEN-LAST:event_btnMinimizarActionPerformed

    private void btnAgregarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnAgregarActionPerformed
NuevoEmpleado newEmpleado = new NuevoEmpleado();
this.setVisible(false);
newEmpleado.setVisible(true);
  
    }//GEN-LAST:event_btnAgregarActionPerformed

    
    //Evento que al dar click nos envia los datos de una determinada fila y los envia a la vista de editar
    private void btnEditarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnEditarActionPerformed

      
      if(txtID.getText().isEmpty()){ 
          JOptionPane.showMessageDialog(null,"Señor usuario seleccione una fila, para editar el empleado");
      }else{ 
          EditarEmpleado vtn = new EditarEmpleado();
          vtn.setVisible(true);
          this.setVisible(false);
          vtn.txtID.setText(txtID.getText());
          vtn.txtPrimerApellido.setText(txtPrimerApellido.getText());
          vtn.txtSegundoApellido.setText(txtSegundoApellido.getText());
          vtn.txtPrimerNombre.setText(txtPrimerNombre.getText());
          vtn.txtOtrosNombres.setText(txtOtrosNombres.getText());
          vtn.txtEmail.setText(txtEmail.getText());
          SimpleDateFormat df = new  SimpleDateFormat("dd-MM-YYYY");
          vtn.txtFechaIngreso.setText(txtFechaIngreso.getText().toString());
          vtn.ComboArea.setSelectedItem(txtArea.getText().toString());
          vtn.ComboPais.setSelectedItem(txtPais.getText().toString());
          vtn.ComboTD.setSelectedItem(txtTD.getText().toString());
      }

    }//GEN-LAST:event_btnEditarActionPerformed
  
    
    
    //Evento que nos permite validar si el usuario esta de acuerdo en eliminar un registro de la BD
    int res;
    private void btnEliminarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnEliminarActionPerformed

        String id=  IdentificacionEmpleado;
        
        if(id == ""){ 
              JOptionPane.showMessageDialog(null,"Señor usuario seleccione una fila");
                 id="";
        }else{ 
                    int opcion=JOptionPane.showConfirmDialog(null,"¿Estas seguro de eliminar el empleado "+NombresApellidos+" ?");
        if(opcion==0){
              res=  opc.eliminarEmpleado(id);
             this.setVisible(false);
       Principal ob= new Principal();
       ob.setVisible(true);
       JOptionPane.showMessageDialog(null,"Eliminacion Exitosa");
        }else{  
            tablaEmpleados.clearSelection();
            id="";
            IdentificacionEmpleado="";
            NombresApellidos="";
            txtID.setText("");
            txtTD.setText("");
            txtPrimerApellido.setText("");
            txtSegundoApellido.setText("");
            txtPrimerNombre.setText("");
            txtOtrosNombres.setText("");
            txtEmail.setText("");
            txtPais.setText("");
            txtFechaIngreso.setText("");
            txtFechaRegistro.setText("");
            txtArea.setText("");
            txtEstado.setText("");
            JOptionPane.showMessageDialog(null,"Eliminacion Cancelada");
        }
        }

    }//GEN-LAST:event_btnEliminarActionPerformed

    //Evento de tipo KeyRealased que permite filtrar los registros de acuerdo al dato previamente ingresado
    private void txtBusquedaKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtBusquedaKeyReleased
        trsfiltro = new TableRowSorter(tablaEmpleados.getModel());
        tablaEmpleados.setRowSorter(trsfiltro);
    }//GEN-LAST:event_txtBusquedaKeyReleased

    private void radiobtnPrimerNombreActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_radiobtnPrimerNombreActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_radiobtnPrimerNombreActionPerformed

    private void jButton1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton1ActionPerformed
                      String cadena = (txtBusqueda.getText());
                txtBusqueda.setText(cadena);
                repaint();
                filtro();
    }//GEN-LAST:event_jButton1ActionPerformed


    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btnAgregar;
    private javax.swing.JButton btnCerrar;
    private javax.swing.JButton btnEditar;
    private javax.swing.JButton btnEliminar;
    private javax.swing.JButton btnMinimizar;
    private javax.swing.ButtonGroup buttonGroup1;
    private javax.swing.JButton jButton1;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel10;
    private javax.swing.JLabel jLabel11;
    private javax.swing.JLabel jLabel12;
    private javax.swing.JLabel jLabel13;
    private javax.swing.JLabel jLabel14;
    private javax.swing.JLabel jLabel15;
    private javax.swing.JLabel jLabel16;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JLabel jLabel7;
    private javax.swing.JLabel jLabel8;
    private javax.swing.JLabel jLabel9;
    private javax.swing.JSeparator jSeparator1;
    public javax.swing.JPanel paginationPanel;
    private javax.swing.JPanel panelConsulta;
    private javax.swing.JPanel panelPrincipal;
    private javax.swing.JRadioButton radiobtnCorreo;
    private javax.swing.JRadioButton radiobtnEstado;
    private javax.swing.JRadioButton radiobtnIdentificacion;
    private javax.swing.JRadioButton radiobtnOtrosNombres;
    private javax.swing.JRadioButton radiobtnPais;
    private javax.swing.JRadioButton radiobtnPrimerApellido;
    private javax.swing.JRadioButton radiobtnPrimerNombre;
    private javax.swing.JRadioButton radiobtnSegundoApellido;
    private javax.swing.JRadioButton radiobtnTipoDocumento;
    public javax.swing.JTable tablaEmpleados;
    public javax.swing.JScrollPane tableSroll;
    public static javax.swing.JTextField txtArea;
    private javax.swing.JTextField txtBusqueda;
    public static javax.swing.JTextField txtEdicion;
    public static javax.swing.JTextField txtEmail;
    public static javax.swing.JTextField txtEstado;
    public static javax.swing.JTextField txtFechaIngreso;
    public static javax.swing.JTextField txtFechaRegistro;
    public static javax.swing.JTextField txtID;
    public static javax.swing.JTextField txtOtrosNombres;
    public static javax.swing.JTextField txtPais;
    public static javax.swing.JTextField txtPrimerApellido;
    public static javax.swing.JTextField txtPrimerNombre;
    public static javax.swing.JTextField txtSegundoApellido;
    public static javax.swing.JTextField txtTD;
    // End of variables declaration//GEN-END:variables


    //Metodo que nos permite llenar los datos provenientes de la base de datos y llenarlos en el JTable
    private ModeloDeTabla crearModeloDeTabla() {
        
        return new ModeloDeTabla<Employee>() {            
            @Override
            public Object getValueAt(Employee employee, int columnas) {
                switch (columnas){                   
                    case 0:
                        return employee.getIdentificacion();
                    case 1:   
                       return employee.getTipodedocumento();
                    case 2:
                        return employee.getPrimerapellido();
                    case 3:
                         return employee.getSegundoapellido();
                           case 4:
                         return employee.getPrimernombre();
                           case 5:
                         return employee.getOtrosnombres();
                           case 6:
                         return employee.getCorreoelectronico();
                           case 7:
                         return employee.getPais();
                           case 8:
                         return employee.getFechaingreso();
                           case 9:
                         return employee.getArea();
                           case 10:
                         return employee.getEstado();
                           case 11:
                         return employee.getFecharegistro();
    case 12:
                         return employee.getFechaedicion();

                }
                return null;
            }


            
            //Metodo que nos permite darle los nombres de las columnas
            @Override
            public String getColumnName(int columnas) {
                switch (columnas) {
                    case 0:
                        return "ID";
                    case 1:
                        return "T.D";
                    case 2:
                        return "Primer Apellido";
                    case 3:
                        return "Segundo Apellido";
                                            case 4:
                        return "Primer Nombre";
                                            case 5:
                        return "Otros nombres";
                                            case 6:
                        return "Email";
                                            case 7:
                        return "Pais";
                                            case 8:
                        return "Fecha de ingreso";
                                            case 9:
                        return "Area";
                                            case 10:
                        return "Estado";
                                            case 11:
                        return "Fecha de registro";
                                             case 12:
                        return "Fecha de edicion";

                }
                return null;                
            }

            //Metodo que nos permite indicar el total de columnas
            @Override
            public int getColumnCount() {
                return 13;
            }

        }; 
    
    }
}
